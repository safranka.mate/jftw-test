import './style.css';

import { Ticker } from 'pixi.js';

import { app } from './context';
import { createStateManager } from './states/manager';

window.onload = async (): Promise<void> => {
  const manager = createStateManager({ next: 'boot' });

  app.ticker = new Ticker();
  app.ticker.add(manager);
  app.start();
};
