import { Container, Sprite } from 'pixi.js';

import { app, gameHeight, objects, textures } from '../context';

const TILE_SIZE = 160;
const MARGIN = 10;
const GRID_SIZE = 3 * TILE_SIZE + 2 * MARGIN;
const X0 = MARGIN;
const Y0 = gameHeight - (GRID_SIZE + MARGIN);

export function buildNumberGrid() {
  const grid = new Container();
  grid.x = X0;
  grid.y = Y0;

  for (let i = 0; i < 9; i++) {
    const col = i % 3;
    const row = Math.trunc(i / 3);

    const tile = new Sprite(textures[`sym${i + 1}`]);
    tile.width = TILE_SIZE;
    tile.height = TILE_SIZE;
    tile.x = col * TILE_SIZE + col * MARGIN;
    tile.y = row * TILE_SIZE + row * MARGIN;

    grid.addChild(tile);
  }

  objects.grid = grid;
  app.stage.addChild(grid);
}
