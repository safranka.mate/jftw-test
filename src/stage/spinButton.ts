import { Container, Sprite } from 'pixi.js';

import { objects, textures } from '../context';

export function buildSpinButton() {
  const spinButton = new Sprite(textures['button']);
  spinButton.visible = false;
  spinButton.anchor.set(0.5);

  objects.spinButton = spinButton;
  (objects.rightSide as Container).addChild(spinButton);
}
