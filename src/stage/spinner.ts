import { Container, Sprite } from 'pixi.js';

import { objects, textures } from '../context';

const SIZE = 266;

export function buildSpinner() {
  const spinner = new Sprite();
  spinner.visible = false;

  spinner.width = SIZE;
  spinner.height = SIZE;
  spinner.anchor.set(0.5);

  objects.spinner = spinner;
  (objects.rightSide as Container).addChild(spinner);
}

export function setSpinner(n: number) {
  const { spinner } = objects;
  (spinner as Sprite).texture = textures[`sym${n}`];
}
