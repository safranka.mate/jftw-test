import { Container, Sprite } from 'pixi.js';

import { app, objects, textures } from '../context';

const LOSE_Y0 = 200;

export function buildLose() {
  const lose = new Sprite(textures['lose']);
  lose.visible = false;

  lose.y = LOSE_Y0;
  lose.anchor.set(0.5);

  objects.lose = lose;
  (objects.rightSide as Container).addChild(lose);
}

export function buildWin() {
  const win = new Sprite(textures['win']);
  win.visible = false;
  win.zIndex = 10;

  objects.win = win;
  app.stage.addChild(win);
}
