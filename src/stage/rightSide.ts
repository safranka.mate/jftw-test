import { Container } from 'pixi.js';

import { app, gameHeight, objects } from '../context';
import { buildLose } from './results';
import { buildSpinButton } from './spinButton';
import { buildSpinner } from './spinner';

const X0 = 800;
const Y0 = gameHeight / 2;

export function buildRightSide() {
  const rightSide = new Container();
  rightSide.x = X0;
  rightSide.y = Y0;

  app.stage.addChild(rightSide);
  objects.rightSide = rightSide;

  buildSpinButton();
  buildSpinner();
  buildLose();
}
