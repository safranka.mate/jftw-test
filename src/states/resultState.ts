import { objects, sounds } from '../context';
import { setSpinner } from '../stage/spinner';
import { ResultParams, StateFactory } from './types';
import { createSineInterpolator, createTimer } from './utils';

const WAIT_TIME = 1;
const BOUNCE_TIME = 0.2;

export const resultState: StateFactory<ResultParams> = ({ result, win }) => {
  const wait = createTimer(WAIT_TIME);
  const scaleTime = createTimer(BOUNCE_TIME);
  const scale = createSineInterpolator({
    min: 1,
    max: 1.2,
    cycles: 0.25,
    phi0: 0.25,
  });

  const { spinner } = objects;
  spinner.visible = true;
  spinner.scale.set(scale(0));
  setSpinner(result);

  const sound = win ? sounds['win'] : sounds['lose'];
  sound.play();

  return () => {
    const { spinner } = objects;
    const { r } = scaleTime();
    spinner.scale.set(scale(r));

    const { done } = wait();
    if (!done) return null;

    return { next: win ? 'win' : 'lose' };
  };
};
