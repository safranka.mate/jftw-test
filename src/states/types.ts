export type GameState = 'boot' | 'select' | 'spin' | 'result' | 'win' | 'lose';

export type SpinParams = {
  numbers: number[];
};

export type ResultParams = {
  result: number;
  win: boolean;
};

export type StateIntent<P = any> = {
  next: GameState;
  params?: P;
};

export type StateUpdater = (delta: number) => StateIntent;

export type StateFactory<P = any> = (params: P) => StateUpdater;

export type Mutable<T> = { value: T };
