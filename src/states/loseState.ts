import { objects } from '../context';
import { StateFactory } from './types';
import { createTimer } from './utils';

const WAIT_TIME = 3;

export const loseState: StateFactory = () => {
  const time = createTimer(WAIT_TIME);

  objects.lose.visible = true;

  return () => {
    const { done } = time();
    if (!done) return null;

    objects.spinner.visible = false;
    objects.lose.visible = false;

    return { next: 'select' };
  };
};
