export type Timer = () => {
  done: boolean;
  current: number;
  r: number;
};

export function createTimer(limit: number): Timer {
  let start = performance.now();

  return () => {
    const now = performance.now();
    const current = Math.min((now - start) / 1000, limit);

    return {
      current,
      done: current >= limit,
      r: current / limit,
    };
  };
}

type SineOptions = {
  min: number;
  max: number;
  cycles: number;
  phi0?: number;
};

export function createSineInterpolator({
  min,
  max,
  cycles,
  phi0 = 0,
}: SineOptions) {
  const ampl = (max - min) / 2;
  const zero = min + ampl;
  const domain = cycles * 2 * Math.PI;
  const offset = phi0 * 2 * Math.PI;

  return (r: number) => zero + Math.sin(offset + r * domain) * ampl;
}
