import { objects } from '../context';
import { StateFactory } from './types';
import { createSineInterpolator, createTimer } from './utils';

const WAIT_TIME = 3;

export const winState: StateFactory = () => {
  const time = createTimer(WAIT_TIME);
  const alpha = createSineInterpolator({ min: 0.8, max: 1, cycles: 5 });

  const { win } = objects;
  win.visible = true;
  win.alpha = alpha(0);

  return () => {
    const { win, spinner } = objects;
    const { done, r } = time();

    win.alpha = alpha(r);

    if (!done) return null;

    spinner.visible = false;
    win.visible = false;

    return { next: 'select' };
  };
};
