import { Sprite } from 'pixi.js';

import { loadImages } from '../boot/images';
import { initCanvas } from '../boot/initCanvas';
import { loadSounds } from '../boot/sounds';
import { app, textures } from '../context';
import { buildNumberGrid } from '../stage/numberGrid';
import { buildWin } from '../stage/results';
import { buildRightSide } from '../stage/rightSide';
import { Mutable, StateFactory } from './types';

export const bootState: StateFactory = () => {
  const done: Mutable<boolean> = { value: false };

  initCanvas();
  load(done);

  return () => {
    if (!done.value) return null;

    const backgroundSprite = Sprite.from(textures['background']);
    app.stage.addChild(backgroundSprite);

    buildNumberGrid();
    buildRightSide();
    buildWin();

    return { next: 'select' };
  };
};

// Internal

async function load(done: Mutable<boolean>) {
  await loadImages();
  await loadSounds();

  done.value = true;
}
