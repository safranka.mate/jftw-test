import { TickerCallback } from 'pixi.js';

import { bootState } from './bootState';
import { loseState } from './loseState';
import { resultState } from './resultState';
import { selectState } from './selectState';
import { spinState } from './spinState';
import { GameState, StateFactory, StateIntent } from './types';
import { winState } from './winState';

const States: Record<GameState, StateFactory> = {
  'boot': bootState,
  'select': selectState,
  'spin': spinState,
  'result': resultState,
  'lose': loseState,
  'win': winState,
};

function getUpdater({ next, params }: StateIntent) {
  const factory = States[next];
  return factory(params);
}

export function createStateManager(init: StateIntent): TickerCallback<void> {
  let update = getUpdater(init);

  return (delta: number) => {
    const intent = update(delta);
    if (intent) {
      update = getUpdater(intent);
    }
  };
}
