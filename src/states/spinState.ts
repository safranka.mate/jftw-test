import { objects, sounds } from '../context';
import { setSpinner } from '../stage/spinner';
import { ResultParams, SpinParams, StateFactory, StateIntent } from './types';
import { createSineInterpolator, createTimer } from './utils';

const SPIN_TIME = 2;

export const spinState: StateFactory<SpinParams> = ({ numbers }) => {
  const wait = createTimer(SPIN_TIME);
  const scale = createSineInterpolator({ min: 0.6, max: 1, cycles: 3 });

  const { spinner } = objects;
  spinner.visible = true;
  spinner.scale.set(scale(0));

  sounds['spin'].play();

  return () => {
    const { done, r } = wait();
    const { spinner } = objects;

    const spinNum = randomNum();
    setSpinner(spinNum);
    spinner.scale.set(scale(r));

    if (!done) return null;

    spinner.visible = false;
    sounds['spin'].stop();

    const result = randomNum();
    const win = numbers.includes(result);

    const intent: StateIntent<ResultParams> = {
      next: 'result',
      params: { win, result },
    };
    return intent;
  };
};

// Internal

const randomNum = () => 1 + Math.trunc(Math.random() * 9);
