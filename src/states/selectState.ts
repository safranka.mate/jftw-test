import { DisplayObject, Sprite } from 'pixi.js';

import { objects, sounds } from '../context';
import { Mutable, SpinParams, StateFactory, StateIntent } from './types';

const MAX_NUMBERS = 5;

export const selectState: StateFactory = () => {
  const numbers = new Set<number>();
  const ready: Mutable<boolean> = { value: false };

  initGrid(numbers);
  initButton(numbers, ready);

  return () => {
    const { grid, spinButton } = objects;

    grid.children.forEach((tile: Sprite, i: number) => {
      fadeSpriteIf(tile, !numbers.has(i + 1));
    });
    fadeSpriteIf(spinButton, numbers.size === 0);

    if (!ready.value) return null;

    teardown();

    const intent: StateIntent<SpinParams> = {
      next: 'spin',
      params: {
        numbers: Array.from(numbers),
      },
    };
    return intent;
  };
};

// Internal

function fadeSpriteIf(sprite: DisplayObject, condition: boolean) {
  sprite.alpha = condition ? 0.5 : 1;
}

function initGrid(numbers: Set<number>) {
  const onclickNumber = (n: number) => {
    if (!numbers.has(n) && numbers.size < MAX_NUMBERS) {
      numbers.add(n);
      sounds['select'].play();
    } else {
      numbers.delete(n);
      sounds['deselect'].play();
    }
  };

  const { grid } = objects;
  grid.children.forEach((tile: Sprite, i: number) => {
    tile.eventMode = 'static';
    tile.cursor = 'pointer';
    tile.onpointerdown = () => onclickNumber(i + 1);
  });
}

function initButton(numbers: Set<number>, ready: Mutable<boolean>) {
  const onclickButton = () => {
    if (numbers.size === 0) return;

    ready.value = true;
  };

  const { spinButton } = objects;
  spinButton.eventMode = 'static';
  spinButton.cursor = 'pointer';
  spinButton.visible = true;
  spinButton.onpointerdown = onclickButton;
}

function teardown() {
  const { grid, spinButton } = objects;

  grid.children.forEach(tile => {
    tile.eventMode = 'none';
    tile.onpointerdown = null;
  });

  spinButton.eventMode = 'none';
  spinButton.visible = false;
  spinButton.onpointerdown = null;
}
