import { Assets, Texture } from 'pixi.js';

import { textures } from '../context';

const Images = [
  'background',
  'blank',
  'button',
  'mystery',
  'sym1',
  'sym2',
  'sym3',
  'sym4',
  'sym5',
  'sym6',
  'sym7',
  'sym8',
  'sym9',
  'win',
  'lose',
];

export async function loadImages() {
  Images.forEach(name => {
    Assets.add({ alias: name, src: `assets/images/${name}.png` });
  });

  const loaded = await Assets.load<Texture>(Images);
  Object.assign(textures, loaded);
}
