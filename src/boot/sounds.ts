import { Sound } from '../audio/sound';
import { sounds } from '../context';

const Sounds = ['select', 'deselect', 'spin', 'win', 'lose'];

export async function loadSounds() {
  await Promise.all(
    Sounds.map(async name => {
      const res = await fetch(`assets/sounds/${name}.ogg`);
      const bytes = await res.arrayBuffer();
      const sound = await Sound.from(bytes);

      sounds[name] = sound;
    }),
  );
}
