import { app, gameHeight, gameWidth } from '../context';

export function initCanvas() {
  document.body.appendChild(app.view as any);

  resizeCanvas();
}

function resizeCanvas(): void {
  const resize = () => {
    app.renderer.resize(window.innerWidth, window.innerHeight);
    app.stage.scale.x = window.innerWidth / gameWidth;
    app.stage.scale.y = window.innerHeight / gameHeight;
  };

  resize();

  window.addEventListener('resize', resize);
}
