import { Application, DisplayObject, Texture } from 'pixi.js';

import { Sound } from './audio/sound';

export const gameWidth = 1136;
export const gameHeight = 640;

export const app = new Application({
  backgroundColor: 0xd3d3d3,
  width: gameWidth,
  height: gameHeight,
});

export const textures: Record<string, Texture> = {};

export const objects: Record<string, DisplayObject> = {};

export const sounds: Record<string, Sound> = {};
