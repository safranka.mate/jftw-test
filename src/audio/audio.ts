export const audio = new AudioContext();

const VOLUME = 0.8;
const gain = audio.createGain();
gain.gain.value = VOLUME;
gain.connect(audio.destination);

export function attachNode(node: AudioNode) {
  node.connect(gain);
}
