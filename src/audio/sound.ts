import { attachNode, audio } from './audio';

export class Sound {
  private buffer: AudioBuffer;
  private node: AudioBufferSourceNode;

  static async from(bytes: ArrayBuffer) {
    const sound = new Sound();
    await sound.init(bytes);
    return sound;
  }

  constructor() {}

  play() {
    if (this.node) return;

    this.node = audio.createBufferSource();
    this.node.buffer = this.buffer;
    this.node.onended = () => {
      this.node = null;
    };

    attachNode(this.node);
    this.node.start();
  }

  stop() {
    if (!this.node) return;

    this.node.stop();
    this.node = null;
  }

  // Internal

  private async init(bytes: ArrayBuffer) {
    this.buffer = await audio.decodeAudioData(bytes);
  }
}
